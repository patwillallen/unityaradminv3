﻿using UnityEngine;
using System.Collections;

public class LoadFault : MonoBehaviour 
{
	private TextAsset textAsset;
	private string url;
	private string rawText;
	public Vector4 info;
	public Vector3 posInfo;
	
	void Awake()
	{
		if (Application.isEditor)
			url = "file:///"+Application.streamingAssetsPath+"/Faults/Figure1.txt";
		else
			url = Application.streamingAssetsPath+"/Faults/Figure1.txt";
		//print ("url: "+url);
		StartCoroutine(FindAsset ());
	}
	
	IEnumerator FindAsset()
	{
		WWW www = new WWW(url);
		
		//Wait for download to complete
		yield return www;
		
		//Load and retrieve the TextAsset
		rawText = www.text;
		ConvertToValues();
		gameObject.GetComponent<FindFault>().savedFault.x = info[0];
		gameObject.GetComponent<FindFault>().savedFault.y = info[1];
		gameObject.GetComponent<FindFault>().savedFault.z = info[2];
		gameObject.GetComponent<FindFault>().savedFault.w = info[3];
		gameObject.GetComponent<FindFault>().faultPosition.x = posInfo[0];
		gameObject.GetComponent<FindFault>().faultPosition.y = posInfo[1];
		gameObject.GetComponent<FindFault>().faultPosition.z = posInfo[2];
	}
	
	void ConvertToValues()
	{
		string str = null;
		string[] strArr = null;
		int count = 0;
		str = rawText;
		char[] splitchar = { ',' };
		strArr = str.Split(splitchar);
		for (count = 0; count <= 4 - 1; count++)
		{
			//print ("info: "+info);
			info[count] = float.Parse(strArr[count]);
		}
		for (count = 4; count <= 7-1;count++)
		{
			int thecount = count - 4;
			posInfo[thecount] = float.Parse(strArr[count]);
		}
	}
}
