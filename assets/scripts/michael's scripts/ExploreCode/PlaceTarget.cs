﻿using UnityEngine;
using System.Collections;

public class PlaceTarget : MonoBehaviour 
{
	public void CreateCrosshair()
	{
		GameObject away = GameObject.FindGameObjectWithTag("crosshair");
		Destroy(away);
		//GameObject go = Instantiate(Resources.Load<GameObject>("Crosshair")) as GameObject;
		GameObject go = Instantiate( (Resources.Load<GameObject>("Crosshair")), new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		
		go.transform.tag = ("crosshair");
		go.transform.position = new Vector3 (0,1,Camera.main.transform.position.z+1.0f);
		go.transform.parent = GameObject.FindGameObjectWithTag("theModel").transform;
		
	}
}
