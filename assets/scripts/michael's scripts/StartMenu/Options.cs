﻿using UnityEngine;
using System.Collections;

public class Options : MonoBehaviour 
{
	/*Explore Variables*/
	public string modelName;
	/*RotationVariables*/
	public bool buttonRotation;//rotate the object in that direction by the "rotateSensitivity" amount each time the button is pressed.
	public bool buttonHoldRotation;//rotate object by "rotateSensitivity" amount while the button is held down.
	public bool touchRotate;//rotate the object using the touchscreen and the "rotateSensitivity" variable dictates how sensitive the ouch controls are.
	public float rotateSensitivity;//used to dictate the speed of the objects' rotation.
	/*General Game Variables*/
	public bool syncToServer;//should the application always sync to server?
	public bool autoLogin;//should the application automatically log the user in based on the previously saved username and password?
	public string username;//the current saved username.
	public string password;//the current saved password.
	public string course;//which course is the user on.
	/*Course Variables*/
	public int courseNum;
	public int figureNum;
	
	void Awake()
	{
		DontDestroyOnLoad(transform.gameObject);
		ReadFromDocument();
		figureNum = 1;
		courseNum = 2;
	}
	
	public void	SaveToDocument()//saves the variables in options to a .txt document in StreamingAssets
	{
		//gameObject.GetComponent<SaveOptions>().WriteToFile();
	}
	public void ReadFromDocument()
	{
		gameObject.GetComponent<OptionsFunctions>().RetrieveOptions();
	}
}
