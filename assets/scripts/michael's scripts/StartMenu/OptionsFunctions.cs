﻿using UnityEngine;
using System.Collections;

public class OptionsFunctions : MonoBehaviour {

	private Options optionsScript;
	public TextAsset options;//the notepad document with all the options in it.
	private string theText;
	
	void Awake()
	{
		
	}
	
	public void RetrieveOptions()
	{
		GameObject temp = GameObject.FindGameObjectWithTag("Options");
		optionsScript = temp.GetComponent<Options>();
		
		
		string[] splitArray =  options.ToString().Split(char.Parse("~")); //Here we assing the splitted string to array by that char
		for (int  i = splitArray.Length; i >= 0; i--)
		{
			switch (i)
			{
			case 0:
				if (splitArray[i] == "true")
					optionsScript.syncToServer = true;
				else
					optionsScript.syncToServer = false;
				break;
			case 1:
				if (splitArray[i] == "true")
					optionsScript.autoLogin = true;
				else
					optionsScript.autoLogin = false;
				break;
			case 2:
				optionsScript.username = splitArray[i];
				break;
			case 3:
				optionsScript.password = splitArray[i];
				break;
			case 4:
				if (splitArray[i] == "true")
					optionsScript.buttonRotation = true;
				else
					optionsScript.buttonRotation = false;
				break;
			case 5:
				if (splitArray[i] == "true")
					optionsScript.buttonHoldRotation = true;
				else
					optionsScript.buttonHoldRotation = false;
				break;
			case 6:
				if (splitArray[i] == "true")
					optionsScript.touchRotate = true;
				else
					optionsScript.touchRotate = false;
				break;
			case 7:
				optionsScript.rotateSensitivity = float.Parse(splitArray[i]);
				break;
			}
		}
	}
}
