﻿using UnityEngine;
using System.Collections;

public class TouchRotate : MonoBehaviour 
{
	private bool touchActive = false; 
	private Vector2 initialTouchLocation;
	private Vector2 currentTouchLocation;
	public float speed = 0.1F;
	void Update() 
	{
		if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved) {
			print ("touch");
			if (touchActive)
			{
				currentTouchLocation = Input.GetTouch(0).deltaPosition;
			}
			else
			{
				initialTouchLocation =Input.GetTouch(0).deltaPosition;
				touchActive = true;
			}
			/*Find xRotation*/
			float xRotation = currentTouchLocation.x - initialTouchLocation.x;
			xRotation = xRotation / Screen.width;
			xRotation= xRotation/2;
			xRotation = 360*xRotation;
			
			/*Find yRotation*/
			float yRotation = currentTouchLocation.y - initialTouchLocation.y;
			yRotation = yRotation / Screen.width;
			yRotation= yRotation/2;
			yRotation = 360*yRotation;
			
	 
			
			gameObject.transform.eulerAngles = new Vector3(
				gameObject.transform.eulerAngles.x + yRotation,
				gameObject.transform.eulerAngles.y - xRotation,
				gameObject.transform.eulerAngles.z
				);
			
			//transform.Translate(-touchDeltaPosition.x * speed, -touchDeltaPosition.y * speed, 0);
		}
		else
		{
			touchActive = false;
			print ("no touch");
		}
	}
}
